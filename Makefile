build:
	$(MAKE) -C dev/ build
	$(MAKE) -C usermode_helper/ build

install:
	$(MAKE) -C dev/ install
	$(MAKE) -C usermode_helper/ install

all: device usermode
