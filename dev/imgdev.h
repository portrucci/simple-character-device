#ifndef BASIC_CHAR_DEV_H
#define BASIC_CHAR_DEV_H

#include <linux/rwsem.h>

#include "char_vec.h"

struct basic_char_dev_state {
  struct rw_semaphore rwsem;
  struct char_vec query;
  struct char_vec result;
  bool iserr;
};

#endif // #ifndef BASIC_CHAR_DEV_H
