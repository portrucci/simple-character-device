#include <linux/types.h>
#include <linux/umh.h>
#include <linux/sched.h>
#include <linux/cred.h>
#include <linux/slab.h>

#include "user_mode_helper.h"
#include "char_vec.h"
#include "linux/gfp_types.h"

#define USERMODE_BINARY_PATH "/usr/local/bin/imgdev_usermode_helper"

static void imgdev_umh_cleanup_func(struct subprocess_info *infop) {
  kfree(infop->argv[1]); // index of query passed user mode helper
  kfree(infop->argv);
}

static void set_cred_privileges(struct cred *credp, kuid_t uid, kgid_t gid) {
  credp->uid = credp->euid = credp->suid = credp->fsuid = uid;
  credp->gid = credp->egid = credp->sgid = credp->fsgid = gid;
}

static int imgdev_umh_init_func(struct subprocess_info *infop, struct cred *new) {
  set_cred_privileges(new, current->cred->uid, current->cred->gid);

  return 0;
}

static char *envp[] = { NULL };

#define ARGV_LEN 3

int start_usermode_query(const struct char_vec *queryp) {
  size_t query_len = char_vec_len(queryp);

  char **argv = kmalloc(sizeof(char *) * ARGV_LEN, GFP_KERNEL);

  if (argv == NULL)
    return -ENOMEM;

  char *query = kmalloc(query_len + 1, GFP_KERNEL); // reserve 1 extra byte for null termination
  if (query == NULL) {
    kfree(argv);
    return -ENOMEM;
  }

  memcpy(query, queryp->data, query_len);
  query[query_len] = '\0';

  argv[0] = USERMODE_BINARY_PATH;
  argv[1] = query;
  argv[2] = NULL;

  struct subprocess_info *sinfo = call_usermodehelper_setup(USERMODE_BINARY_PATH, argv, envp, GFP_KERNEL, imgdev_umh_init_func, imgdev_umh_cleanup_func, NULL);

  call_usermodehelper_exec(sinfo, UMH_NO_WAIT);

  return 0;
}
