#ifndef CHAR_VEC_H
#define CHAR_VEC_H

#include <linux/types.h>

struct char_vec {
  size_t _len;
  size_t _capacity;
  char *data;
};

int char_vec_init(struct char_vec *vec);
void char_vec_destroy(struct char_vec *vec);
int char_vec_push(struct char_vec *vec, char item);
int char_vec_extend(struct char_vec *vec, const char *items, size_t len);
int char_vec_set(struct char_vec *vec, const char *source, size_t len);
inline size_t char_vec_len(const struct char_vec *vec);
int char_vec_grow(struct char_vec *, size_t);

/// sets the vec's size to be `n` less, or 0 if `n` is bigger than the current size.
/// *does not free memory*
inline void char_vec_trim(struct char_vec *vec, size_t n);

#endif
