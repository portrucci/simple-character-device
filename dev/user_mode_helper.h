#ifndef USER_MODE_HELPER_H
#define USER_MODE_HELPER_H

#include "char_vec.h"

// starts the user mode helper that performs the actual query for the image
// query should be a null-terminated string
int start_usermode_query(const struct char_vec *query);

#endif // #ifndef USER_MODE_HELPER_H
