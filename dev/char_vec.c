#include <linux/slab.h>

#include "char_vec.h"

#define CHAR_VEC_INITIAL_SIZE 10
#define CHAR_VEC_SCALE_FACTOR 2

int char_vec_init(struct char_vec *vec) {
  vec->_len = 0;

  return char_vec_grow(vec, CHAR_VEC_INITIAL_SIZE);
}

void char_vec_destroy(struct char_vec *vec) {
  kfree(vec->data);
  vec->_len = 0;
  vec->_capacity = 0;
}

int char_vec_grow(struct char_vec *vec, size_t size) {
  if (size <= vec->_capacity)
    return 0;

  char *data = kmalloc(size, GFP_KERNEL);
  if (data == NULL)
    return 1;

  vec->data && memcpy(data, vec->data, vec->_len);

  kfree(vec->data);

  vec->data = data;
  vec->_capacity = size;

  return 0;
}

int char_vec_push(struct char_vec *vec, char item) {
  if (vec->_capacity < vec->_len + 1 &&
          char_vec_grow(vec, vec->_capacity * CHAR_VEC_SCALE_FACTOR))
    return 1;

  vec->data[vec->_len] = item;
  vec->_len++;

  return 0;
}

int char_vec_extend(struct char_vec *vec, const char *items, size_t len) {
  if (vec->_capacity < vec->_len + len &&
          char_vec_grow(vec, vec->_capacity + len)) // pre-allocate more than size + length of new data
    return 1;

  memcpy(vec->data + vec->_len, items, len);
  vec->_len += len;

  return 0;
}

int char_vec_set(struct char_vec *vec, const char *source, size_t len) {
  if (char_vec_grow(vec, len))
    return 1;

  memcpy(vec->data, source, len);
  vec->_len = len;

  return 0;
}

inline size_t char_vec_len(const struct char_vec *vec) {
  return vec->_len;
}

inline void char_vec_trim(struct char_vec *vec, size_t n) {
  vec->_len = n > vec->_len ? 0 : vec->_len - n;
}
