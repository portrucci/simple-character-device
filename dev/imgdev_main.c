#include <linux/module.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/rwsem.h>

#include "imgdev.h"
#include "imgdev_ioctl.h"
#include "char_vec.h"
#include "user_mode_helper.h"

MODULE_LICENSE("AGPL");

char *test_data = "this is a test";

struct miscdevice miscdev;

static struct basic_char_dev_state state = {0};

static int miscdev_open(struct inode *inode, struct file *file) {
  printk(KERN_DEBUG "Entering %s\n", __func__);

  switch (file->f_mode & (FMODE_READ | FMODE_WRITE)) {
  case FMODE_READ: {
    // don't block on already locked semaphore if O_NONBLOCK is set
    if (file->f_flags & O_NONBLOCK) {
      if (!down_read_trylock(&state.rwsem))
        return -EWOULDBLOCK;
    } else {
      down_read(&state.rwsem);
    }

    break;
  }
  case FMODE_WRITE: {
    // same here
    if (file->f_flags & O_NONBLOCK) {
      if (!down_write_trylock(&state.rwsem))
        return -EWOULDBLOCK;
    } else {
      down_write(&state.rwsem);
    }

    break;
  }
  default:
    return -EINVAL;
  }

  return 0;
}

static int miscdev_release(struct inode *inode, struct file *file) {
  printk(KERN_DEBUG "Entering %s\n", __func__);

  switch (file->f_mode & (FMODE_READ | FMODE_WRITE)) {
  case FMODE_READ: {
    up_read(&state.rwsem);
    break;
  }
  case FMODE_WRITE: {
    if (start_usermode_query(&state.query)) {
      state.iserr = true;
    }
    up_write(&state.rwsem);
    break;
  }
  default:
    break;
  }
  return 0;
}

static long miscdev_ioctl(struct file *file, unsigned int cmd, unsigned long arg) {
  printk(KERN_DEBUG "Entering %s\n", __func__);

  switch (cmd) {
  case IOCTL_SET_IMAGE_RESULT: {
    struct user_buf_info ubuf;
    if (unlikely(copy_from_user(&ubuf, (struct user_buf_info *)arg, sizeof(struct user_buf_info))))
      return -EFAULT;

    char *temp_buf = kmalloc(ubuf.size, GFP_KERNEL);

    if (unlikely(copy_from_user(temp_buf, ubuf.data, ubuf.size)))
      return -EFAULT;

    if (unlikely(temp_buf == NULL) || char_vec_set(&state.result, temp_buf, ubuf.size))
      return -ENOMEM;

    kfree(temp_buf);

    state.iserr = false;
    break;
  }
  case IOCTL_SET_ERROR:
    state.iserr = true;
    break;
  default:
    return -EINVAL;
  }

  return 0;
}

static ssize_t miscdev_read(struct file *file, char __user *buf, size_t count, loff_t *offset) {
  printk(KERN_DEBUG "Entering %s\n", __func__);
  if (*offset < 0)
    return -EFAULT;

  if (*offset >= char_vec_len(&state.result))
    return 0;

  if (state.iserr) {
    // if the user mode helper returned an error then *most likely* we have a network problem.
    // TODO: check for more error types
    return -ENONET;
  }

  count = char_vec_len(&state.result) < count + *offset
              ? char_vec_len(&state.result) - *offset
              : count;
  if (unlikely(copy_to_user(buf, state.result.data + *offset, count))) {
    return -EFAULT;
  }

  *offset += count;


  return count;
}

static ssize_t miscdev_write(struct file *file, const char __user *buf, size_t count, loff_t *offset) {
  printk(KERN_DEBUG "Entering %s\n", __func__);

  if (*offset < 0)
    return -EINVAL;

  char *temp_buf = kmalloc(count, GFP_KERNEL);

  if (unlikely(temp_buf == NULL))
    return -ENOMEM;

  if(unlikely(copy_from_user(temp_buf, buf, count)))
    return -EFAULT;

  char_vec_trim(&state.query, char_vec_len(&state.query) - *offset);
  char_vec_extend(&state.query, temp_buf, count);

  kfree(temp_buf);

  *offset += count;

  return count;
}

static const struct file_operations miscdev_ops = {
  .owner = THIS_MODULE,
  .open = miscdev_open,
  .release = miscdev_release,
  .unlocked_ioctl = miscdev_ioctl,
  .read = miscdev_read,
  .write = miscdev_write
};

static int __init imgdev_init_module(void) {
  int retval;

  printk(KERN_DEBUG "Entering %s\n", __func__);

  miscdev.minor = MISC_DYNAMIC_MINOR;
  miscdev.name = "imgdev.jpg";
  miscdev.mode = 0666;
  miscdev.fops = &miscdev_ops;

  retval = misc_register(&miscdev);
  if(unlikely(retval < 0)) {
    printk(KERN_ERR "failed to register misc device! error: %d\n", retval);
    return retval;
  }

  init_rwsem(&state.rwsem);

  printk(KERN_DEBUG "dev's minor number is %d\n", miscdev.minor);

  if (unlikely(char_vec_init(&state.query) || char_vec_init(&state.result))) {
    printk(KERN_ERR "failed to allocate memory for user data");
    return -ENOMEM;
  }
  return 0;
}

static void __exit imgdev_cleanup_module(void) {
  printk(KERN_DEBUG "Entering %s\n", __func__);
  misc_deregister(&miscdev);
  char_vec_destroy(&state.query);
  char_vec_destroy(&state.result);
}

module_init(imgdev_init_module);
module_exit(imgdev_cleanup_module);
