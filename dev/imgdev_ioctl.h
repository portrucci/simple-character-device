#ifndef CDEV_IOCTL_H
#define CDEV_IOCTL_H

#include <linux/ioctl.h>
#include <linux/types.h>

#define IOCTL_BASE '!'

struct user_buf_info {
  size_t size;
  char __user *data;
};

#define IOCTL_SET_IMAGE_RESULT             _IOW(IOCTL_BASE, 0x22, struct user_buf_info)

#define IOCTL_SET_ERROR                    _IO(IOCTL_BASE, 0x23)

#endif // #ifnded CDEV_IOCTL_H
