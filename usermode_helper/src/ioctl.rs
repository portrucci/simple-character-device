use std::{fs::File, os::fd::{AsRawFd, IntoRawFd}};
use nix::{ioctl_none, ioctl_write_ptr};

const IOCTL_MAGIC_NUMBER: u8 = b'!';
const IMGDEV_PATH: &str = "/dev/imgdev.jpg";

#[repr(C)]
#[derive(Debug)]
struct ResultBuffer {
    size: usize,
    data: *const u8,
}

ioctl_write_ptr!(write_query_result, IOCTL_MAGIC_NUMBER, 0x22, ResultBuffer);
ioctl_none!(set_error_true, IOCTL_MAGIC_NUMBER, 0x23);

pub fn imgdev_set_error_true() {
    let f = File::open(IMGDEV_PATH).unwrap();
    unsafe { let _ = set_error_true(f.into_raw_fd()); };
}

pub fn write_result_to_imgdev(data: &[u8]) {
    let f = File::open(IMGDEV_PATH).unwrap(); // unwrap because there is nothing we can do if we can't open the file.
    let buffer = ResultBuffer {
        size: data.len(),
        data: data.as_ptr(),
    };

    let fd = f.as_raw_fd();
    unsafe {
        if let Err(_) = write_query_result(fd, &buffer as *const _) {
            let _ = set_error_true(fd);
        }
    };
}
