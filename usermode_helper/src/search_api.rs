use serde::{Deserialize, Serialize};
use reqwest::header::USER_AGENT;

const GOOGLE_SEARCH_API_KEY: &str = std::env!("GOOGLE_SEARCH_API_KEY");
const GOOGLE_SEARCH_ENGINE_ID: &str = std::env!("GOOGLE_SEARCH_ENGINE_ID");

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
struct ImageResult {
    link: String,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
struct SearchApiResponse {
    items: Box<[ImageResult]>,
}

fn get_google_image_results(query: &str) -> Option<SearchApiResponse> {
    let url = format!("https://customsearch.googleapis.com/customsearch/v1?key={GOOGLE_SEARCH_API_KEY}&q={query}&cx={GOOGLE_SEARCH_ENGINE_ID}&searchType=image&fileType=jpg");
    let response = reqwest::blocking::get(url);

    response
        .ok()?
        .json()
        .ok()
}

fn get_image_from_url(url: &str) -> Option<Box<[u8]>> {
    let client = reqwest::blocking::Client::new();
    Some(client.get(url)
         .header(USER_AGENT, "imgdev/0.1") // some sites don't return a response without a user agnet
         .send()
         .ok()?
         .bytes()
         .ok()?
         .into_iter()
         .collect())
}

pub fn get_image_response(query: &str) -> Option<Box<[u8]>> {
    let response = get_google_image_results(query)?;
    if response.items.len() == 0 { return None; }

    get_image_from_url(&response.items[0].link)
}
