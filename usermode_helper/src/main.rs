use std::env::args;

use imgdev_usermode_helper::{get_image_response, imgdev_set_error_true, write_result_to_imgdev};

fn main() {
    if let Some(img) = args().nth(1).and_then(|query| get_image_response(&query)) {
        write_result_to_imgdev(&img);
    } else {
        imgdev_set_error_true();
    }
}
